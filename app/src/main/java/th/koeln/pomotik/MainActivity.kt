package th.koeln.pomotik

import android.content.Context
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import th.koeln.pomotik.screens.help.HelpScreenComponent
import th.koeln.pomotik.screens.start.StartBildschirm
import th.koeln.pomotik.screens.timer.TimerScreenComponent
import th.koeln.pomotik.screens.todo.TodoListScreen
import th.koeln.pomotik.ui.theme.PomoTikTheme

class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val sharedPrefs = getSharedPreferences("app_prefs", Context.MODE_PRIVATE)
        val isFirstLaunch = sharedPrefs.getBoolean("first_launch", true)

        setContent {
            PomoTikTheme {
                // Navigation Controller aufrufen und den Wert für den ersten Start übergeben
                NavigationController(isFirstLaunch)
            }
        }

        // Setze den First Launch Flag auf false
        if (isFirstLaunch) {
            sharedPrefs.edit().putBoolean("first_launch", false).apply()
        }
    }

    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun NavigationController(isFirstLaunch: Boolean) {
        val navController = rememberNavController()

        // Dynamische Startdestination abhängig vom ersten Start der App
        val startDestination = if (isFirstLaunch) "startseite" else "timer-activity"

        NavHost(navController = navController, startDestination = startDestination) {
            composable("startseite") { StartBildschirm(navController) }
            composable("timer-activity") { TimerScreenComponent(navController) }
            composable("todo-list") { TodoListScreen(navController) }
            composable("help") { HelpScreenComponent(navController) }
        }
    }
}
