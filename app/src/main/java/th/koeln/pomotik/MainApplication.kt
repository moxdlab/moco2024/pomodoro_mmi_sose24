//package th.koeln.pomotik
//
//import android.app.Application
//import androidx.room.Room
//import th.koeln.pomotik.screens.db.AppDatabase
//
//class MainApplication : Application() {
//
//    companion object {
//        lateinit var todoDatabase: AppDatabase
//    }
//
//    override fun onCreate() {
//        super.onCreate()
//        todoDatabase = Room.databaseBuilder(
//            applicationContext,
//            AppDatabase::class.java,
//            "task_database"
//        ).build()
//    }
//
//}