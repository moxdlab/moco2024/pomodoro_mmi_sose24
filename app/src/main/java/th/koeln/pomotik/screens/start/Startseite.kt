package th.koeln.pomotik.screens.start

import android.annotation.SuppressLint
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonColors
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import th.koeln.pomotik.R
import th.koeln.pomotik.ui.theme.PomoTikTheme


@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun StartBildschirm(navHostController: NavHostController) {
    val figmaBackgroundColor = Color(0xFFF7F2FA) // HEX-Code von Figma

    Scaffold(
        Modifier.background(figmaBackgroundColor), containerColor = figmaBackgroundColor
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(figmaBackgroundColor),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            ShowStartseite()
        }

        Column {
            Button(
                onClick = {
                navHostController.navigate("timer-activity")
                },
                modifier =
                Modifier
                    .offset(x = 150.dp, y = 700.dp)
                    .size(100.dp, 50.dp),
                shape = RoundedCornerShape(20.dp),
                colors = ButtonColors(
                    Color(0xFF6750A4),
                    Color.White,
                    Color.Gray,
                    Color.Gray
                )
            ) {
                Text(text = "Start")
            }
        }
    }
}

@Composable
fun ShowStartseite() {
    val pomotikImage = painterResource(id = R.drawable.pomotik)
    val tomatoImage = painterResource(id = R.drawable.tomato)

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxSize()
    ) {
        Image(
            painter = pomotikImage,
            contentDescription = "Pomotik",
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 180.dp)
                .width(300.dp)
                .height(150.dp),
            contentScale = ContentScale.Fit,
            alignment = Alignment.TopCenter
        )

        Image(
            painter = tomatoImage,
            contentDescription = "Tomato",
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 150.dp)
                .width(150.dp)
                .height(120.dp),
            contentScale = ContentScale.Fit,
            alignment = Alignment.TopCenter
        )

        Spacer(modifier = Modifier.height(70.dp)) // Abstand zwischen Bild und Button
    }
}

@RequiresApi(Build.VERSION_CODES.TIRAMISU)
@Preview
@Composable
fun PreviewBildschirm() {

    PomoTikTheme {
        StartBildschirm(NavHostController(LocalContext.current))
    }
}