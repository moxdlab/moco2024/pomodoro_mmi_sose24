package th.koeln.pomotik.screens.timer

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class TimerViewModel : ViewModel() {
    var totalTime by mutableIntStateOf(25 * 60) // Initiale Zeit in Sekunden
    var timeLeft by mutableIntStateOf(totalTime) // Verbleibende Zeit
    var isRunning by mutableStateOf(false)

    fun startTimer() {
        isRunning = true
        viewModelScope.launch {
            while (isRunning && timeLeft > 0) {
                delay(1000L)
                timeLeft -= 1
            }
            isRunning = false
        }
    }

    fun pauseTimer() {
        isRunning = false
    }

    fun resetTimer() {
        isRunning = false
        timeLeft = totalTime
    }

    fun setTime(newTime: Int) {
        totalTime = newTime
        timeLeft = totalTime
    }
}
