package th.koeln.pomotik.screens.db

import androidx.lifecycle.LiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import th.koeln.pomotik.screens.db.Task

class TaskRepository(private val taskDao: TaskDAO) {
    val allTasks: LiveData<List<Task>> = taskDao.getAllTasks()

    suspend fun insert(task: Task) {
        withContext(Dispatchers.IO) {
            taskDao.insertTask(task)
        }
    }

    suspend fun update(task: Task) {
        withContext(Dispatchers.IO) {
            taskDao.updateTask(task)
        }
    }

    suspend fun delete(taskId: Int) {
        withContext(Dispatchers.IO) {
            taskDao.deleteTask(taskId)
        }
    }
}
