package th.koeln.pomotik.screens.timer

import NotificationService
import android.app.Activity
import android.content.pm.PackageManager
import android.media.MediaPlayer
import android.os.Build
import android.widget.NumberPicker
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.BottomAppBar
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarColors
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.core.app.ActivityCompat
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import kotlinx.coroutines.delay
import th.koeln.pomotik.R


@ExperimentalMaterial3Api
@Composable
fun TimerScreenComponent(navHostController: NavHostController) {
    var totalTime by remember { mutableIntStateOf(25 * 60) } // Initiale Zeit in Sekunden
    var timeLeft by remember { mutableIntStateOf(totalTime) } // Verbleibende Zeit
    var isRunning by remember { mutableStateOf(false) }
    var showDialog by remember { mutableStateOf(false) } // Steuerung für den Dialog
    val context = LocalContext.current // Context abrufen
    val notificationService = NotificationService(context)
    val timerViewModel: TimerViewModel = viewModel()

    // MediaPlayer für den Alarmton
    val mediaPlayer = remember {
        MediaPlayer.create(context, R.raw.alarm_4) // Audio aus res/raw
    }


    LaunchedEffect(isRunning) {
        while (isRunning && timeLeft > 0) {
            delay(1000L)
            timeLeft -= 1


            notificationService.showTimerNotification {
                formatTime(timeLeft)
            }

            if (timeLeft == 0) {
                isRunning = false
                mediaPlayer.start() // Alarmton abspielen
            }
        }
    }

    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.Center
                    ) {
                        Text(
                            text = "PomoTik",
                            color = Color.White,
                            fontWeight = FontWeight.Bold
                        )
                    }
                },
                colors = TopAppBarColors(
                    containerColor = Color(0xFF4A4AFC),
                    Color(0xFF4A4AFC),
                    Color(0xFF4A4AFC),
                    Color(0xFF4A4AFC),
                    Color(0xFF4A4AFC)
                ),


                )
        },
        bottomBar = {
            BottomAppBar(
                containerColor = Color(0xFF4A4AFC),
                contentColor = Color.White
            ) {
                Button(
                    onClick = {
                        // Navigiere zur Todo-Liste
                        navHostController.navigate("todo-list")
                    },
                    modifier = Modifier
                        .weight(1f)
                        .padding(8.dp)
                ) {
                    Text(
                        text = "Todo Liste",
                        textAlign = TextAlign.Center
                    )
                }

                Button(
                    onClick = {
                        // Zeige Hilfeseite oder Dialog an
                        navHostController.navigate("help")
                    },
                    modifier = Modifier
                        .weight(1f)
                        .padding(8.dp)
                ) {
                    Text(
                        text = "Help",
                        textAlign = TextAlign.Center
                    )
                }
            }
        }
    ) { innerPadding ->
        Modifier.padding(innerPadding)
        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(
                    Brush.verticalGradient(
                        colors = listOf(
                            Color(0xFF7DABD8),
                            Color(0xFFD2BBE8)
                        )
                    )
                )
                .padding(16.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Button(
                onClick = { showDialog = true }, // Dialog anzeigen
                enabled = !timerViewModel.isRunning,
                modifier = Modifier
                    .fillMaxWidth(0.5f)
                    .height(50.dp),
                colors = ButtonDefaults.buttonColors(containerColor = Color(0xFF2424C2)),
                shape = RoundedCornerShape(20.dp)
            ) {
                Text(
                    text = "Zeit ändern",
                    color = Color.White,
                    fontWeight = FontWeight.Bold,
                    fontSize = 18.sp
                )
            }

            Spacer(modifier = Modifier.height(30.dp))

            Text(
                text = formatTime(timerViewModel.timeLeft),
                fontSize = 72.sp,
                fontWeight = FontWeight.Bold,
                color = Color.White,
                textAlign = TextAlign.Center
            )

            Spacer(modifier = Modifier.height(40.dp))

            Row(
                horizontalArrangement = Arrangement.SpaceEvenly,
                modifier = Modifier.fillMaxWidth()
            ) {
                Button(
                    onClick = { timerViewModel.startTimer() },
                    enabled = !timerViewModel.isRunning,
                    modifier = Modifier
                        .width(80.dp)
                        .height(80.dp),
                    colors = ButtonDefaults.buttonColors(containerColor = Color(0xFF4CAF50)),
                    shape = RoundedCornerShape(20.dp)
                ) {
                    Text(text = "Start", color = Color.White, fontWeight = FontWeight.Bold)
                }

                Button(
                    onClick = { timerViewModel.pauseTimer() },
                    enabled = timerViewModel.isRunning,
                    modifier = Modifier
                        .width(90.dp)
                        .height(80.dp),
                    colors = ButtonDefaults.buttonColors(containerColor = Color(0xFFF44336)),
                    shape = RoundedCornerShape(20.dp)
                ) {
                    Text(text = "Pause", color = Color.White, fontWeight = FontWeight.Bold)
                }

                Button(
                    onClick = { timerViewModel.resetTimer() },
                    modifier = Modifier
                        .size(85.dp),
                    colors = ButtonDefaults.buttonColors(containerColor = Color(0xFF9E9E9E)),
                    shape = RoundedCornerShape(20.dp)
                ) {
                    Text(text = "Reset", color = Color.White, fontWeight = FontWeight.Bold)
                }
            }
        }

        if (showDialog) {
            PickerDialog(
                initialMinutes = timerViewModel.totalTime / 60,
                onTimeSelected = { minutes ->
                    timerViewModel.setTime(minutes * 60)
                    showDialog = false
                },
                onDismissRequest = { showDialog = false }
            )
        }
    }
}

// Dialog für die Zeiteinstellung
@Composable
fun PickerDialog(
    initialMinutes: Int,
    onTimeSelected: (Int) -> Unit,
    onDismissRequest: () -> Unit
) {
    var selectedMinutes by remember { mutableIntStateOf(initialMinutes) } // Aktuelle Auswahl des NumberPickers

    // UI für den Dialog
    AlertDialog(
        onDismissRequest = onDismissRequest,
        title = { Text(text = "Zeit auswählen") },
        text = {
            Column(
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                // Native Android NumberPicker verwenden
                AndroidView(
                    factory = { context ->
                        NumberPicker(context).apply {
                            minValue = 1 // Min. Zeit 1 Minute
                            maxValue = 60 // Max. Zeit 60 Minuten
                            value = initialMinutes

                            setOnValueChangedListener { _, _, newVal ->
                                selectedMinutes = newVal // Update bei Veränderung
                            }
                        }
                    }
                )
            }
        },
        confirmButton = {
            Button(onClick = { onTimeSelected(selectedMinutes) }) { // Bestätige die Auswahl
                Text("OK")
            }
        },
        dismissButton = {
            Button(onClick = onDismissRequest) {
                Text("Abbrechen")
            }
        }
    )
}

fun formatTime(time: Int): String {
    val minutes = time / 60
    val seconds = time % 60
    return String.format("%02d:%02d", minutes, seconds)
}


@OptIn(ExperimentalMaterial3Api::class)
@Preview(showBackground = true)
@Composable
fun TimerScreenActivityPreview() {
    TimerScreenComponent(NavHostController(LocalContext.current))

}
