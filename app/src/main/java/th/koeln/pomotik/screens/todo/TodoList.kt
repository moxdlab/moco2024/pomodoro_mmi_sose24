package th.koeln.pomotik.screens.todo

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.text.font.FontWeight
import androidx.lifecycle.LiveData
import androidx.navigation.NavHostController
import th.koeln.pomotik.screens.db.AppDatabase
import th.koeln.pomotik.screens.db.Task
import th.koeln.pomotik.screens.db.TaskRepository
import th.koeln.pomotik.screens.db.TaskViewModel
import java.text.SimpleDateFormat
import java.util.Date
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.text.style.TextDecoration


@Composable
fun TaskItem(
    item: Task,
    onRemove: (Task) -> Unit,
    onCheckedChange: (Task, Boolean) -> Unit
) {
    val dateFormat = SimpleDateFormat("dd.MM.yyyy")



    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(6.dp),
        shape = RoundedCornerShape(16.dp),
        colors = CardDefaults.cardColors(
            containerColor = Color.White
        )
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Column(modifier = Modifier.weight(1f)) {
                Text(
                    text = item.name,
                    fontSize = 18.sp,
                    fontWeight = FontWeight.Bold,
                    color = if (item.isCompleted) Color.Gray else Color.Black,
                    textDecoration = if (item.isCompleted) TextDecoration.LineThrough else TextDecoration.None // Text wird durchgestrichen, wenn abgeschlossen
                )
                Text(
                    text = dateFormat.format(item.date),
                    style = MaterialTheme.typography.bodyMedium,
                    color = Color.Gray
                )
            }


            // Checkbox zum Markieren des Tasks als abgeschlossen
            Checkbox(
                checked = item.isCompleted,
                onCheckedChange = { isChecked ->
                    onCheckedChange(item, isChecked)
                }
            )

            Spacer(modifier = Modifier.width(8.dp))

            // Button zum Entfernen des Tasks
            IconButton(onClick = { onRemove(item) }) {
                Icon(
                    tint = Color(0xFFF44336),
                    imageVector = Icons.Default.Delete,
                    contentDescription = "Remove Task"
                )
            }
        }
    }
}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TodoListScreen(navHostController: NavHostController) {
    var newItem by remember { mutableStateOf("") }
    val taskViewModel = TaskViewModel(TaskRepository(AppDatabase.getDatabase(LocalContext.current).taskDao()))

    // LiveData beobachten und in todoItems speichern
    val todoItems by taskViewModel.allTasks.observeAsState(listOf())

    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(
                        text = "Todo Liste",
                        color = Color.White,
                        fontWeight = FontWeight.Bold
                    )
                },
                navigationIcon = {
                    IconButton(onClick = { navHostController.popBackStack() }) {
                        Icon(
                            imageVector = Icons.Default.ArrowBack,
                            contentDescription = "Zurück",
                            tint = Color.White
                        )
                    }
                },
                colors = TopAppBarDefaults.mediumTopAppBarColors(
                    containerColor = Color(0xFF4A4AFC),
                    scrolledContainerColor = Color(0xFF4A4AFC),
                    actionIconContentColor = Color(0xFF4A4AFC),
                    navigationIconContentColor = Color(0xFF4A4AFC),
                    titleContentColor = Color(0xFF4F2929)
                )
            )
        }
    ) { innerPadding ->
        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(
                    Brush.verticalGradient(
                        colors = listOf(
                            Color(0xFF7DABD8),
                            Color(0xFFD2BBE8)
                        )
                    )
                )
                .padding(innerPadding),
            verticalArrangement = Arrangement.SpaceBetween
        ) {
            // Todo Items List
            LazyColumn(
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(1f)
            ) {
                items(todoItems) { item ->
                    TaskItem(
                        item = item,
                        onCheckedChange = { updatedItem, isChecked ->
                            taskViewModel.update(updatedItem.copy(isCompleted = isChecked))
                        },
                        onRemove = { removedItem ->
                            taskViewModel.delete(removedItem.id)
                        }
                    )
                }
            }

            // Input Field
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp)
                    .background(Color.White, RoundedCornerShape(8.dp))
                    .padding(8.dp)
                    .align(Alignment.End)
            ) {
                BasicTextField(
                    value = newItem,
                    onValueChange = { newItem = it },
                    keyboardOptions = KeyboardOptions.Default.copy(
                        imeAction = ImeAction.Done
                    ),
                    keyboardActions = KeyboardActions(
                        onDone = {
                            if (newItem.isNotBlank()) {
                                val newTask = Task(name = newItem, date = Date(), isCompleted = false)
                                taskViewModel.insert(newTask)
                                newItem = ""
                            }
                        }
                    ),
                    modifier = Modifier
                        .weight(1f)
                        .padding(16.dp)
                )

                Spacer(modifier = Modifier.width(8.dp))

                Button(
                    onClick = {
                        if (newItem.isNotBlank()) {
                            val newTask = Task(name = newItem, date = Date(), isCompleted = false)
                            taskViewModel.insert(newTask)
                            newItem = ""
                        }
                    }
                ) {
                    Text(text = "Hinzufügen")
                }
            }
        }
    }
}


@Composable
@Preview
fun Preview() {
    TodoListScreen(navHostController = NavHostController(LocalContext.current))
}