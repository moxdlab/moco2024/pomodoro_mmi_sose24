import android.app.NotificationManager
import android.content.Context
import androidx.core.app.NotificationCompat

class NotificationService(
    private val context: Context,
) {
    private val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    private val notificationId = 1 // Feste ID, um die Benachrichtigung zu aktualisieren

    fun showTimerNotification(remainingTime: () ->  String) {
        val notification = NotificationCompat.Builder(context, "pomotik")
            .setSilent(true)
            .setContentTitle("Timer läuft...")
            .setContentText("Verbleibende Zeit: ${remainingTime()}")
            .setSmallIcon(android.R.drawable.ic_notification_overlay) // Setze dein eigenes Icon
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .build()

        notificationManager.notify(notificationId, notification)
    }
}
